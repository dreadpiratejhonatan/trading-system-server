﻿namespace TradingSystemServer {
    using NLog;
    using QuickFix;
    using System;
    using System.Threading;
    using TradingSystemServer.OperationalAndHistoricalData;
    using TradingSystemServer.Orders;
    using TradingSystemServer.OrdersEntry;

    public class ServerApplication {
        static Logger _logger = LogManager.GetCurrentClassLogger();                

        public void Start() {
            try {
                OrdersEntryGateway.Instance.Start();
                OrdersManager.Instance.Start();
            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }

        public void Stop() {
            try {

            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }
    }
}