﻿namespace TradingSystemServer.OrdersEntry {
    using NLog;
    using QuickFix;
    using System;
    using TradingSystemServer.Applications;

    class OrdersEntryGateway {
        static OrdersEntryGateway instance = new OrdersEntryGateway();
        static readonly object syncLock = new object();
        static Logger _logger = LogManager.GetCurrentClassLogger();
        ThreadedSocketAcceptor _acceptor;
        SessionSettings _acceptorSettings;
        AcceptorApp _acceptorApp;

        private OrdersEntryGateway() {
            _acceptorApp = new AcceptorApp();
            _acceptorSettings = new ServerApplicationConfig().GetAcceptorSessionSettings();
            InitializeAcceptor();
        }

        public static OrdersEntryGateway Instance {
            get {
                lock (syncLock) {
                    return instance;
                }
            }
        }

        public void SendExecutionReport(Message msg, SessionID sessionID) {
            _acceptorApp.SendExecutionReport(msg, sessionID);
        }

        public void InitializeAcceptor() {
            IMessageStoreFactory storeFactory = new FileStoreFactory(_acceptorSettings);
            ILogFactory logFactory = new FileLogFactory(_acceptorSettings);
            _acceptor = new ThreadedSocketAcceptor(_acceptorApp, storeFactory, _acceptorSettings, logFactory);
        }

        public void Start() {
            try {
                _acceptor.Start();
                _logger.Info($"{nameof(OrdersEntryGateway)} is running as acceptor");
            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }

        public void Stop() {
            try {
                _acceptor.Stop();
                _logger.Info($"{nameof(OrdersEntryGateway)} has been stopped");
            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }
    }
}
