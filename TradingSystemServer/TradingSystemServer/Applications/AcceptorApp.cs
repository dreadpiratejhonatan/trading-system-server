﻿namespace TradingSystemServer.Applications {
    using NLog;
    using QuickFix;
    using QuickFix.Fields;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Text;
    using TradingSystemServer.Orders;
    using NewOrderSingle = QuickFix.FIX44.NewOrderSingle;
    using Reject = QuickFix.FIX44.Reject;

    class AcceptorApp : IApplication {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        ConcurrentDictionary<string, Session> _sessions;

        public AcceptorApp() {
            _sessions = new ConcurrentDictionary<string, Session>();
        }

        public void OnCreate(SessionID sessionID) {
            _logger.Info($"Session created: (targetCompID) {sessionID.TargetCompID} <-> {sessionID.SenderCompID} (senderCompID)");
            _sessions[sessionID.TargetCompID] = Session.LookupSession(sessionID);
        }

        public void FromApp(Message msg, SessionID sessionID) {
            _logger.Info($"Msg received from session {sessionID.TargetCompID} <-> {sessionID.SenderCompID}: {msg.ToString()}");
            if (!IsMessageValid(msg, out var rejection)) {
                SendReject(msg, sessionID, rejection);
                return;
            }
            OrdersManager.Instance.HandleOrderRequest(msg, sessionID);
        }

        public bool IsMessageValid(Message msg, out string rejection) {
            rejection = string.Empty;
            if (msg == null) {
                rejection = $"Message is null";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.ClOrdID))) {
                rejection = $"Message doesn't contain ClOrdID";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.Side))) {
                rejection = $"Message doesn't contain Side";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.Symbol))) {
                rejection = $"Message doesn't contain Symbol";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.SecurityID))) {
                rejection = $"Message doesn't contain SecurityID";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.OrderQty))) {
                rejection = $"Message doesn't contain OrderQty";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.Price))) {
                rejection = $"Message doesn't contain Price";
                return false;
            }
            if (string.IsNullOrEmpty(msg.GetField(Tags.Price))) {
                rejection = $"Message doesn't contain Price";
                return false;
            }
            if ((msg.GetField(Tags.MsgType) == "G" ||
                 msg.GetField(Tags.MsgType) == "F") &&
                 string.IsNullOrEmpty(msg.GetField(Tags.OrigClOrdID))) {
                rejection = $"Message doesn't contain OrigClOrdID";
                return false;
            }
            return string.IsNullOrEmpty(rejection);
        }

        private void SendReject(Message msg, SessionID sessionID, string rejection) {
            Reject rejectMsg = new Reject();
            rejectMsg.SetField(new StringField(Tags.ClOrdID, msg.GetField(Tags.ClOrdID)));
            rejectMsg.SetField(new StringField(Tags.Text, rejection));
            _sessions[sessionID.TargetCompID].Send(rejectMsg);
        }

        public void SendExecutionReport(Message msg, SessionID sessionID) {
            _sessions[sessionID.TargetCompID].Send(msg);
        }

        public void OnLogon(SessionID sessionID) {
            _logger.Info($"Session logon: {sessionID.ToString()}");
        }

        public void OnLogout(SessionID sessionID) {
            _logger.Info($"Session logout: {sessionID.ToString()}");
        }

        public void FromAdmin(Message msg, SessionID sessionID) {
            _logger.Info($"From admin: {msg.ToString()}");
        }

        public void ToAdmin(Message msg, SessionID sessionID) {
            _logger.Info($"To admin: {msg.ToString()}");
        }

        public void ToApp(Message msg, SessionID sessionID) {
            _logger.Info($"To app: {msg.ToString()}");
        }
    }
}
