﻿namespace TradingSystemServer.Applications {
    using NLog;
    using QuickFix;
    using QuickFix.Fields;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using TradingSystemServer.OperationalAndHistoricalData;
    using TradingSystemServer.Orders;
    using NewOrderSingle = QuickFix.FIX44.NewOrderSingle;
    using Reject = QuickFix.FIX44.Reject;

    class InitiatorApp : IApplication {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        Session _matchingEngineSession;

        public InitiatorApp() {
            _matchingEngineSession = null;
        }

        public void OnCreate(SessionID sessionID) {
            _logger.Info($"Session created: (targetCompID) {sessionID.TargetCompID} <-> {sessionID.SenderCompID} (senderCompID)");
            _matchingEngineSession = Session.LookupSession(sessionID);
        }

        public void FromApp(Message msg, SessionID sessionID) {
            switch (msg.GetField(Tags.MsgType)) {
                case "8":
                    OrdersManager.Instance.HandleExecutionReport(msg, sessionID);
                    break;
            }
        }

        public void OnLogon(SessionID sessionID) {
            _logger.Info($"Session logon: {sessionID.ToString()}");
        }

        public void OnLogout(SessionID sessionID) {
            _logger.Info($"Session logout: {sessionID.ToString()}");
        }

        public void FromAdmin(Message msg, SessionID sessionID) {
            _logger.Info($"From admin: {msg.ToString()}");
        }

        public void ToAdmin(Message msg, SessionID sessionID) {
            _logger.Info($"To admin: {msg.ToString()}");
        }

        public void ToApp(Message msg, SessionID sessionID) {
            _logger.Info($"To app: {msg.ToString()}");
        }

        public void SendOrder(Message order) {
            if (_matchingEngineSession == null) {
                return;
            }
            _matchingEngineSession.Send(order);
        }
    }
}
