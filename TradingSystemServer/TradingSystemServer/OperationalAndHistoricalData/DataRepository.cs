﻿namespace TradingSystemServer.OperationalAndHistoricalData {
    using NLog;
    using QuickFix;
    using QuickFix.Fields;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    sealed class DataRepository {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        static DataRepository instance = new DataRepository();
        static readonly object syncLock = new object();

        private ConcurrentDictionary<string, List<Message>> _operationalData;
        private ConcurrentDictionary<string, List<Message>> _historicalData;

        private DataRepository() {
            _operationalData = new ConcurrentDictionary<string, List<Message>>();
            _historicalData = new ConcurrentDictionary<string, List<Message>>();
        }

        public static DataRepository Instance {
            get {
                lock (syncLock) {
                    return instance;
                }
            }
        }

        public void StoreOperationalData(Message msg, SessionID sessionID) {
            var targetCompId = sessionID.TargetCompID;
            if (!_operationalData.ContainsKey(targetCompId)) {
                _operationalData[targetCompId] = new List<Message>();
            }
            _operationalData[targetCompId].Add(msg);
        }

        internal void StoreOperationalData(Message msg, object sessionID) {
            throw new NotImplementedException();
        }

        public void StoreHistoricalData(Message msg) {
            var secId = msg.GetField(Tags.SecurityID);
            if (!_historicalData.ContainsKey(secId)) {
                _historicalData[secId] = new List<Message>();
            }
            _historicalData[secId].Add(msg);
        }
    }
}
