﻿namespace TradingSystemServer {
    using System;
    using NLog;
    using Topshelf;

    class Program {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        [STAThread]
        static void Main(string[] args) {
            try {
                HostFactory.Run(hostConfig => {
                    hostConfig.Service<ServerApplication>(serviceConfig => {
                        serviceConfig.ConstructUsing(() => new ServerApplication());
                        serviceConfig.WhenStarted(s => s.Start());
                        serviceConfig.WhenStopped(s => s.Stop());
                    });
                    hostConfig.RunAsLocalSystem();
                    hostConfig.SetDescription("Trading System Server");
                    hostConfig.SetDisplayName("Trading System Server");
                    hostConfig.SetServiceName("Trading System Server");
                });
            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }
    }
}