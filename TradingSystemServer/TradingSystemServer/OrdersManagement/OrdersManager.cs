﻿namespace TradingSystemServer.Orders {
    using NLog;
    using QuickFix;
    using QuickFix.Fields;
    using NewOrderSingle = QuickFix.FIX44.NewOrderSingle;
    using OrderCancelReplaceRequest = QuickFix.FIX44.OrderCancelReplaceRequest;
    using OrderCancelRequest = QuickFix.FIX44.OrderCancelRequest;
    using System;
    using TradingSystemServer.Applications;
    using SocketInitiator = QuickFix.Transport.SocketInitiator;
    using TradingSystemServer.OperationalAndHistoricalData;
    using System.Collections.Concurrent;
    using TradingSystemServer.OrdersEntry;

    sealed class OrdersManager {
        static Logger _logger = LogManager.GetCurrentClassLogger();
        static OrdersManager instance = new OrdersManager();
        static readonly object syncLock = new object();
        SocketInitiator _initiator;
        SessionSettings _initiatorSettings;
        InitiatorApp _initiatorApp;
        ConcurrentDictionary<string, Tuple<SessionID, Message>> _orders;

        private OrdersManager() {
            _orders = new ConcurrentDictionary<string, Tuple<SessionID, Message>>();
            _initiatorApp = new InitiatorApp();
            _initiatorSettings = new ServerApplicationConfig().GetInitiatorSessionSettings();
            InitializeInitiator();
        }

        public static OrdersManager Instance {
            get {
                lock (syncLock) {
                    return instance;
                }
            }
        }

        public void HandleOrderRequest(Message msg, SessionID sessionID) {
            DataRepository.Instance.StoreOperationalData(msg, sessionID);
            Message order = new Message();
            switch (msg.GetField(Tags.MsgType)) {
                case "D":
                    order = GenerateNewOrderSingleRequest(msg);
                    break;
                case "G":
                    order = GenerateOrderReplaceRequest(msg);
                    break;
                case "F":
                    order = GenerateOrderCancelRequest(msg);
                    break;
            }
            _orders[order.GetField(Tags.ClOrdID)] = new Tuple<SessionID, Message>(sessionID,order);
            SendOrderRequest(order);
        }

        public void HandleExecutionReport(Message msg, SessionID sessionID) {
            DataRepository.Instance.StoreOperationalData(msg, sessionID);
            OrdersEntryGateway.Instance.SendExecutionReport(msg, sessionID);
        }

        private void SendOrderRequest(Message msg) {
            _initiatorApp.SendOrder(msg);
        }

        private Message GenerateNewOrderSingleRequest(Message msg) {
            NewOrderSingle order = new NewOrderSingle();
            var clOrdID = Guid.NewGuid().ToString();
            order.ClOrdID = new ClOrdID(clOrdID);
            return order;
        }

        private Message GenerateOrderReplaceRequest(Message msg) {
            OrderCancelReplaceRequest order = new OrderCancelReplaceRequest();
            var clOrdID = Guid.NewGuid().ToString();
            order.ClOrdID = new ClOrdID(clOrdID);
            return order;
        }

        private Message GenerateOrderCancelRequest(Message msg) {
            OrderCancelRequest order = new OrderCancelRequest();
            var clOrdID = Guid.NewGuid().ToString();
            order.ClOrdID = new ClOrdID(clOrdID);
            return order;
        }

        private void InitializeInitiator() {
            IMessageStoreFactory storeFactory = new FileStoreFactory(_initiatorSettings);
            ILogFactory logFactory = new ScreenLogFactory(_initiatorSettings);
            _initiator = new SocketInitiator(_initiatorApp, storeFactory, _initiatorSettings, logFactory);
        }

        public void Start() {
            try {
                _initiator.Start();
                _logger.Info($"{nameof(OrdersManager)} is running as initiator");
            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }

        public void Stop() {
            try {
                _initiator.Stop();
                _logger.Info($"{nameof(OrdersManager)} has been stopped");
            }
            catch (Exception e) { _logger.Info($"{e.Message} | {e.StackTrace}"); }
        }
    }
}
