﻿namespace TradingSystemServer {
    using Newtonsoft.Json;
    using QuickFix;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ServerApplicationConfig {
        private SessionSettings AcceptorSettings { get; set; }
        private SessionSettings InitiatorSettings { get; set; }
        private ServerApplicationConfig Config { get; set; }

        public ServerApplicationConfig() {

        }

        public SessionSettings GetAcceptorSessionSettings() {
            if (AcceptorSettings == null) {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config\acceptorSessionSettings.cfg");
                AcceptorSettings = new SessionSettings(path);
            }
            return AcceptorSettings;
        }

        public SessionSettings GetInitiatorSessionSettings() {
            if (InitiatorSettings == null) {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config\initiatorSessionSettings.cfg");
                InitiatorSettings = new SessionSettings(path);
            }
            return InitiatorSettings;
        }

        public ServerApplicationConfig GetServerApplicationConfig() {
            if (Config == null) {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config\serverApplication.json");
                using (StreamReader r = new StreamReader(path)) {
                    string json = r.ReadToEnd();
                    Config = JsonConvert.DeserializeObject<ServerApplicationConfig>(json);
                }
            }
            return Config;
        }
    }
}
